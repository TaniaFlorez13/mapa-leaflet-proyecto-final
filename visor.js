function initMap(){

    var mymap=L.map('visor', {center:[4.625770, -74.172777], zoom:11});
     
    /*L.tileLayer('https://api.tiles.mapbox.com/v4{id}/{z}/{x}/{y},png?access_token=pk.eyJ1ljoibWFwYm94liwiYSl6lmNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214ArilSLbB6B5aw',{
        maxZoom:20,
        id:'mapbox.streets'
    }).addTo(mymap);*/
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'sk.eyJ1IjoidGFuaWFmbG9yZXoiLCJhIjoiY2tjd2NoOXhtMDdrODJ6c3p3cDBjMzZ0eCJ9.R63m1IU2JHjkycmoROR1hg'
}).addTo(mymap);

/*Marcador Parque Salitre Mágico
var marker = L.marker([4.66805556, -74.0913889]).addTo(mymap);
marker.bindPopup("<b>Parque Salitre Mágico</b><br>¡Diversión extrema!.").openPopup();*/
    
/*Marcador Parque Tercer Milenio
var marker = L.marker([4.5975, -74.08166667]).addTo(mymap);
marker.bindPopup("<b>Parque Tercer Milenio</b>").openPopup();*/


//Marcador de Parque El galán
var marker = L.marker([4.716933097646829,-74.09994542598724]).addTo(mymap);
marker.bindPopup("<b>Parque El Galán</b>").openPopup();

//Marcador de Parque San Andrés
var marker = L.marker([ 4.71291805272128, -74.11076009273529]).addTo(mymap);
marker.bindPopup("<b>Parque San Andrés </b>").openPopup();

/*Marcador de Parque Cici AquaPark
var marker = L.marker([ 4.670360362250434, -74.08902883529663]).addTo(mymap);
marker.bindPopup("<b>Parque Cici AquaPark</b>").openPopup();*/

//Marcador de Parque Villa Luz
var marker = L.marker([ 4.682181608929356, -74.1085284948349]).addTo(mymap);
marker.bindPopup("<b>Parque Villa Luz</b>").openPopup();


//Marcador de Parque Villa Gladys
var marker = L.marker([ 4.703334779204806, -74.13505285978317]).addTo(mymap);
marker.bindPopup("<b>Parque Villa Gladys</b>").openPopup();

//Marcador de Parque de Engativá
var marker = L.marker([ 4.715131409252221, -74.14186835289001]).addTo(mymap);
marker.bindPopup("<b>Parque de Engativá</b>").openPopup();

//Marcador de Parque El Real
var marker = L.marker([ 4.682694874277517, -74.10203754901886]).addTo(mymap);
marker.bindPopup("<b>Parque El Real</b>").openPopup();

//Marcador de Parque Los Ángeles
var marker = L.marker([ 4.695710836727867, -74.1207244992256]).addTo(mymap);
marker.bindPopup("<b>Parque Los Angeles</b>").openPopup();

/*Marcador de Parque Virgilio Barco
var marker = L.marker([ 4.655603573957321, -74.0890234708786]).addTo(mymap);
marker.bindPopup("<b>Parque Virgilio Barco</b>").openPopup();*/

//Marcador de Parque La Florida
var marker = L.marker([ 4.715676727894544, -74.10655975341797]).addTo(mymap);
marker.bindPopup("<b>Parque La Florida</b>").openPopup();

//Marcador de Parque El Tabora
var marker = L.marker([ 4.693286234465508, -74.0972900390625]).addTo(mymap);
marker.bindPopup("<b>Parque El Tabora</b>").openPopup();

//Marcador de Parque Ciudadela El Colsubsidio
var marker = L.marker([ 4.725032618266966, -74.10839974880219]).addTo(mymap);
marker.bindPopup("<b>Parque Ciudadela El Colsubsidio</b>").openPopup();

//Marcador de Parque Deportivo El Salitre
var marker = L.marker([ 4.66498165575986, -74.09767627716064]).addTo(mymap);
marker.bindPopup("<b>Parque Deportivo El Salitre</b>").openPopup();

//Marcador de Parque Deportivo El Salitre
var marker = L.marker([ 4.66498165575986, -74.09767627716064]).addTo(mymap);
marker.bindPopup("<b>Parque Deportivo El Salitre</b>").openPopup();



var polygon = L.polygon([
    [4.65346488328521, -74.10338401794434],
    [4.684475260534678, -74.12467002868652],
    [4.688517201692816, -74.11831855773926],
    [4.692003089837433, -74.12050724029541],
    [4.721942515285971, -74.16020393371582],
    [4.740157, -74.126206],
    [4.726076, -74.104145],
    [4.711060, -74.090951],
    [4.685773, -74.076949]
]).addTo(mymap);


}   